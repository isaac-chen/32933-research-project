# UTS 32933 Research Project

**Use Reinforcement Learning to Classify the Text Language in the News/Advertisement Image**

Author: Lijian Chen

The Reinforcement Learning algorithm used in this repository is based on the paper [Image Classification by Reinforcement Learning with Two-State Q-Learning]

This repository is created and modified based on the GitHub repository [ilonatommy / DLR_FacialEmotionRecognition]

[Image Classification by Reinforcement Learning with Two-State Q-Learning]: <https://arxiv.org/ftp/arxiv/papers/2007/2007.01298.pdf>

[ilonatommy / DLR_FacialEmotionRecognition]: <https://github.com/ilonatommy/DLR_FacialEmotionRecognition>
